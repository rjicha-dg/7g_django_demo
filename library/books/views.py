from django.shortcuts import render
from .models import Book


def book_list(request):    
    books = Book.objects.all()
    context = {
        'books': books
    }
    return render(request, 'books/index.html', context)

def book_detail(request, id):
    book = Book.objects.filter(id=id).first()
    context = {
        'book': book
    }
    return render(request, 'books/detail.html', context)
