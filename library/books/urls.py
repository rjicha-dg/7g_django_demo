from . import views
from django.urls import path

urlpatterns = [
    path('', views.book_list),
    path('detail/<int:id>/', views.book_detail, name='book_detail')
]